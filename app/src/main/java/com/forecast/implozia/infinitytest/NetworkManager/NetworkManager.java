package com.forecast.implozia.infinitytest.NetworkManager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import rx.Observable;

/**
 * Created by Viper on 20.09.2017.
 */

public class NetworkManager {
    public Observable<Boolean> getNetworkState(Context context) {
        return Observable.create(
                subscriber -> {
                    Runnable r = () -> {
                        while (!subscriber.isUnsubscribed())
                            subscriber.onNext(hasConnection(context));
                    };
                    new Thread(r).start();
                }
        );
    }

    private static boolean hasConnection(final Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return wifiInfo != null && wifiInfo.isConnected() || mobileInfo != null && mobileInfo.isConnected();
    }
}
