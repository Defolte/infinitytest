package com.forecast.implozia.infinitytest.Activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.forecast.implozia.infinitytest.NetworkManager.NetworkManager;
import com.forecast.implozia.infinitytest.R;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Handler handlerNetwork = new Handler();

    private Button button;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeViews();

        try {rxNetworkCheck();
        } catch (Exception ignored) {}
    }

    private void initializeViews() {
        button = (Button)findViewById(R.id.button);
        textView = (TextView)findViewById(R.id.textView);

    }

    private void rxNetworkCheck(){
        new NetworkManager().getNetworkState(this).subscribe(x ->
                handlerNetwork.post(() -> {
                    if(x){
                        button.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.INVISIBLE);
                    }
                    else{
                        button.setVisibility(View.INVISIBLE);
                        textView.setVisibility(View.VISIBLE);
                    }
                })
        );
    }
}
